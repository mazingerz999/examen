<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

 DB::table('transportista')->delete();
 $this->call(TransportistaSeeder::class);

   
// DB::table('users')->delete();
// $this->call(UserSeeder::class);
// \App\Models\User::factory(5)->create();
    }
}
