<?php

namespace Database\Factories;

use App\Models\Paquete;
use Illuminate\Database\Eloquent\Factories\Factory;

class PaqueteFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Paquete::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
   public function definition() {
        
        return [
            'direccion' => $this->faker->secondaryAddress ,
            'direccion' => $this->faker->true ,
            'imagen' => $this->faker->imageUrl($width = 640, $height = 480) // 'http://lorempixel.com/640/480/',

        ];
    }
}
