<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransportistaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transportista', function (Blueprint $table) {
            $table->id();
            $table->String('nombre',30);
            $table->String('apellidos',30);
            $table->string('slug');
            $table->String('fechaPermisoConducir',30);
            $table->String('imagen');
            $table->unsignedBigInteger("empresa_id");
            $table->unsignedBigInteger("paquete_id");
            $table->timestamps();
          $table->foreign("empresa_id")->references("id")->on("empresa");
          $table->foreign("paquete_id")->references("id")->on("paquete");
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transportista');
    }
}
