<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransportistaEmpresaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transportista_empresa', function (Blueprint $table) {
           
            $table->unsignedBigInteger('empresa_id');
            $table->unsignedBigInteger('transportista_id');
            $table->foreign("empresa_id")->references("id")->on("empresa");
            $table->foreign("transportista_id")->references("id")->on("transportista");            
            $table->primary(array("empresa_id", "transportista_id"));
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transportista_empresa');
    }
}
