<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [\App\Http\Controllers\InicioController::class, 'inicio'] )->name('home');

Route::get('transportistas', [\App\Http\Controllers\TransportistaController::class, 'index'])->name('transportistas.index');

Route::get('transportistas/{transportista}/entregar', [\App\Http\Controllers\TransportistaController::class, 'entregar'])->name('transportistas.entregar');

Route::get('transportistas/{transportista}/noentregar', [\App\Http\Controllers\TransportistaController::class, 'noentregar'])->name('transportistas.noentregar');

Route::get('paquetes/{transportista}/create', [\App\Http\Controllers\RevisionController::class, 'create'])->name('paquetes.create');

Route::post('paquetes/{transportista}', [\App\Http\Controllers\RevisionController::class, 'store'])->name('paquetes.store');