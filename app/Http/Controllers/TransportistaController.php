<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TransportistaController extends Controller
{
    
    
        public function index() {
        $transportistas = \App\Models\Transportista::all();
        // return view("animales.index", ['animales'=> $this->animales]);
        return view("transportistas.index", compact("transportistas"));
    }
    


    public function show(\App\Models\Transportista $transportista) {
        
        return view("transportistas.show", ["transportista" => $transportista]);
        //  return view("animales.show", ['animal'=> $this->animales[$animal]]);
    }

    public function edit(\App\Models\Transportista $transportista) {
        
        return view("transportistas.edit", ["transportista" => $transportista]);
    }
    
    public function entregar(\App\Models\Transportista $transportista) {
        
        $transportista= \App\Models\Transportista::findorFail($transportista);
        if ($transportista->get) {
            
        }
        
        return view("transportistas.entregar", ["transportista" => $transportista]);
    }
    public function noentregar(\App\Models\Transportista $transportista) {
        
        return view("transportistas.noentregar", ["transportista" => $transportista]);
    }

}
