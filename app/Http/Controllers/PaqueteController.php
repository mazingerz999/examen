<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PaqueteController extends Controller
{
    
        public function create(\App\Models\Transportista $transportista)
    {
         return view("paquetes.create", ["transportista" => $transportista]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, \App\Models\Transportista $transportista)
    {

        //Esto me coge todos los datos del formulario
        
        $datos=$request->all();
        $datos['transportista_id'] = $transportista->id;
        $paquete= \App\Models\Paquete::create($datos);
        return redirect()->route("animales.show", $$transportista)->with("mensaje", "Ha habido un error");
    }
}
