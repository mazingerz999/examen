<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transportista extends Model
{
    use HasFactory;
        protected $table = "transportista";
    public $timestamps = false;

    public function paquete() {
        return $this->belongsTo(Paquete::class);
    }

      public function empresa()
      {
      return $this->belongsToMany(Empresa::class);
      
      } 
}
