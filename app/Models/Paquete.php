<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Paquete extends Model
{
    use HasFactory;
        protected $table= "paquete";
    
    public $timestamps=false;
    
        public function transportista() {
        return $this->hasMany(Transportista::class);
        
    }
}
