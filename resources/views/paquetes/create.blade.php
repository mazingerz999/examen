@extends('layouts.master')
@section('titulo')
    Zoológico
@endsection
@section('contenido')
    <h1>Revisiones</h1>
    <div class="row">
 <div class="offset-md-3 col-md-6">
 <div class="card">
 <div class="card-header text-center">
     <h2> Añadir nuevo paquete</h2>
 </div>
 <div class="card-body" style="padding:30px">
     
 <form method="post" action="{{route('paquetes.store', $paquete)}}" enctype="multipart/form-data">

@csrf 

 <div class="form-group">
 
 <label for="direccion">Direccion</label>
 <input type="text" name="direccion" id="direccion"  class="form-control" required>
 </div>

 <div class="form-group">
  <label for="transportista">Transportista</label>
 <select id="transportista" name="transportista">
     @foreach($transportista as $transpor)
     <option value="{{$transpor->id}}">{{$transpor->nombre}}</option>
 </select>
</div>
 <div class="form-group">
{{-- TODO: Completa el input para la alimentación--}}
 <label for="imagen">Imagen</label>
 <input type="file" name="imagen" value="imagen" id="imagen" class="form-control" required>
 </div>
 </div>
 <div class="form-group text-center">
 <button type="submit" class="btn btn-success" style="padding:8px 100px;margin-top:25px;">
 Añadir Revision
 </button>
 </div>
 {{-- TODO: Cerrar formulario --}}
 </form>
 </div>
 </div>
 </div>
@endsection
