@extends('layouts.master')
@section('titulo')
   Transportistas
@endsection
@section('contenido')

  <div class="container  mb-5 py-3 ">
    <div class="card border-success rounded-3  mb-1 p-3 m-5">
      <div class="row ">
        <div class="col-md-4   ">
           <img src="{{asset('assets/imagenes')}}/{{$transportista->logo}}"  class="w-100"/>
          </div>
          <div class="col-md-8 px-3">
            <div class="card-block px-3">
              <h2 class="card-title">Nombre: {{$transportista->nombre}}</h2>
              <h5 class="card-text">Autor:  {{$transportista->autor}}</h5>
              <h6 class="card-text">{{$transportista->descripcion}} </h6>
                <h6 class="card-text"> Nº Paginas: {{$transportista->paginas}}</h6>

                  <a href="{{route('comics.edit' , $transportista )}}" class="btn btn-success btn-lg">Editar</a>
            </div>
          </div>

        </div>
      </div>
    </div>

@endsection