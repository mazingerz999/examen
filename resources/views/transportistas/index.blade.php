@extends('layouts.master')
@section('titulo')
    Listado de Transportistas
@endsection
@section('contenido')
<div class="wrapper">
    <div class="typing-demo">
  
    </div>
</div>
 <div class="row justify-content-center">
      
    <!-- Recorro con un for each el array de animales -->
@foreach( $transportistas as  $transportista)
<div class="card  border-success mb-1 p-2 m-3" style="width: 19rem;">
    <!-- Cojo la ruta animal y uso la imagen -->
<img src="{{asset('assets/imagenes')}}/{{$transportista->imagen}}" style="height:300px"/>
 
<div class="card-body">

    <h5 class="card-title"">- Nombre: {{$transportista->nombre}}</h5>
    <p class="card-text">- Paquetes:  {{count($transportista->paquete)}} Pendientes</p>

    <hr>
  <a href="{{route('transportista.show' , $transportista )}}"  class="btn btn-success">Ver + Info</a>

  </div>
</div>
@endforeach
</div>
@endsection